%global goipath github.com/sachaos/todoist
Version: 0.15.0
%gometa

%global _docdir_fmt %{name}

%global golicenses LICENSE
%global godocs *.md
%global godevelheader %{expand:
# The devel package will usually benefit from corresponding project binaries.
Requires:  %{name} = %{version}-%{release}
}

%global common_description %{expand:
Todoist is a cool to-do list web application. This program will let you use Todoist in the CLI.
}

Name: todoist
Release: 1%{?dist}
Summary: A command line client for Todoist
License: MIT
URL: %{gourl}
Source0: %{gosource}
%description
%{common_description}

%gopkg

%prep
%goprep

%generate_buildrequires
%go_generate_buildrequires

%build
%gobuild -o %{gobuilddir}/bin/todoist

%install
%gopkginstall
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/

%dnl %check
%dnl %gocheck

%files
%license %{golicenses}
%doc
%{_bindir}/*

%gopkgfiles

%changelog
* Mon Jul 06 13:32:00 UTC 2020 Thomas Drake-Brockman <thomas@drake-brockman.id.au> - 0.15.0-1
- Initial package

